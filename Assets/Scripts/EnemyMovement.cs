﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyMovement : MonoBehaviour
{
    [SerializeField] float movementPeriod = 0.5f;
    [SerializeField] ParticleSystem goalParticle;

    void Start()
    {
        Pathfinder pathfinder = FindObjectOfType<Pathfinder>();
        var path = pathfinder.GetPath();
        StartCoroutine(FollowPath(path));
    }

    
    IEnumerator FollowPath(List<Waypoint> path)
    {
        foreach (Waypoint waypoint in path)
        {
            while (Vector3.Distance(transform.position, waypoint.transform.position) > 0.1f)
            {
                transform.position = Vector3.MoveTowards(transform.position, waypoint.transform.position, Time.deltaTime * 10);
                yield return new WaitForEndOfFrame();
            }
            //yield return new WaitForSeconds(movementPeriod);
        }
        SelfDestruct();
    }

  
    private void SelfDestruct()
    {
        var vfx = Instantiate(goalParticle, transform.position, Quaternion.identity);
        vfx.Play();

        float destroyDelay = vfx.main.duration;

        Destroy(vfx.gameObject, destroyDelay);

        Destroy(gameObject);
    }


}
