﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyDamage : MonoBehaviour
{
    [SerializeField] Collider collishionMesh;
    [SerializeField] int hitPoints = 10;
    [SerializeField] ParticleSystem hitParticlePrefab, deathParticlePrefab;

    private void OnParticleCollision(GameObject other)
    {
        print("I'm hit!");
        ProcessHit();
        if (hitPoints < 1)
        {
            KillEnemy();
        }
    } 
    void ProcessHit()
    {
        hitPoints = hitPoints-1;
        hitParticlePrefab.Play();
    }
    private void KillEnemy()
    {
        var vfx = Instantiate(deathParticlePrefab, transform.position, Quaternion.identity);
        vfx.Play();

        float destroyDelay = vfx.main.duration;

        Destroy(vfx.gameObject, destroyDelay);

        Destroy(gameObject);
    }

}
