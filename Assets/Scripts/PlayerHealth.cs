﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlayerHealth : MonoBehaviour
{
    [SerializeField] int currentHealth = 10;
    [SerializeField] int healthDecrease = 1;
    [SerializeField] Text healthText;

    void Start()
    {
        healthText.text = currentHealth.ToString();
    }
    private void OnTriggerEnter(Collider other)
    {
        currentHealth = Mathf.Clamp(currentHealth-1, 0, 10);
        healthText.text = currentHealth.ToString();
        if (currentHealth == 0)
        {
            healthText.text = "YOU DIED";
        }

    }
}
