﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class EnemySpawner : MonoBehaviour
{
    [Range(0.1f, 120f)]
    [SerializeField] float secondsBetweenSpawns = 5f;
    [SerializeField] EnemyMovement enemyPrefab;
    [SerializeField] Transform enemyParentTransform;
    [SerializeField] Text spawnedEnemies;
    int score;
    bool startdecreasing = true;

    void Start()
    {
        StartCoroutine(RepeatedlySpawnEnemies());
        spawnedEnemies.text = score.ToString();
    }

    IEnumerator RepeatedlySpawnEnemies()
    {
        while (true)
        {
            score++;
            DecraseSpawnTime();
            IncreaseSpawnTime();
            spawnedEnemies.text = score.ToString();
            var newEnemy = Instantiate(enemyPrefab, transform.position, Quaternion.identity);
            newEnemy.transform.parent = enemyParentTransform;
            yield return new WaitForSeconds(secondsBetweenSpawns);
        }

    }
    void DecraseSpawnTime()
    {
        if (startdecreasing == true )
        {
            secondsBetweenSpawns = secondsBetweenSpawns - 0.2f;
        }
        if (secondsBetweenSpawns <= 0.6f)
        {
            startdecreasing = false;
        }

    }
    void IncreaseSpawnTime()
    {
        if (startdecreasing == false)
        {
            secondsBetweenSpawns = secondsBetweenSpawns + 0.4f;
        }
        if (secondsBetweenSpawns > 5)
        {
            startdecreasing = true;
        }

    }

}
